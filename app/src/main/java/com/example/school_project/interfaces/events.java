package com.example.school_project.interfaces;


import com.example.school_project.Player;

public class events {
    public interface GameTurnEvent {

        void on(Player oldPlayerTurn, Player turnedPlayer);
    }
    public interface GameStartedEvent {

        void on(Player currentPlayer);
    }
}
