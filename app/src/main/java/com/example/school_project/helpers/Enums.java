package com.example.school_project.helpers;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Enums {
    public enum PlayerLevel {
        HAUT,
        MOYEN,
        FAIBLE,
    }

    public static <T> String[] ToArrayString(Class<T> e) {

        Object[] enumConsytant = e.getEnumConstants();
        if(enumConsytant == null) {
            return new String[0];
        }
        return  Arrays.stream(enumConsytant).map(_e->_e.toString().trim()).toArray(String[]::new);
    }
    }
