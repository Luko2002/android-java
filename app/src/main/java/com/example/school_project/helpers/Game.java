package com.example.school_project.helpers;

import com.example.school_project.Player;
import com.example.school_project.interfaces.events;
import com.example.school_project.R;

import java.io.Serializable;
import java.util.function.Function;

public class Game implements Serializable {

    protected Player player1;
    protected Player player2;

    protected Integer MagicNumber;

    protected  Player currentPlayer;

    protected  Player oldPlayer;
    protected boolean start;

    private events.GameTurnEvent handleTurnEvent;
    private events.GameStartedEvent handleStartedEvent;

    public Game(String playerName1, String player1Level, String playerName2,  String player2Level) {

        this.player1 = new Player(playerName1, Enums.PlayerLevel.valueOf(player1Level));
        this.player2 = new Player(playerName2, Enums.PlayerLevel.valueOf(player2Level));
    }


    public void setOnGameTurnEvent(events.GameTurnEvent event) {
        this.handleTurnEvent = event;
    };

    public void setOnGameStarted(events.GameStartedEvent event) {
        this.handleStartedEvent = event;
    };

    private void switchUser() {
        if(this.currentPlayer.hashCode() == this.player1.hashCode()) {
            this.currentPlayer = this.player2;
        }else {
            this.currentPlayer = this.player1;
        }
    }

    public void Turn(Integer trialN) {

        this.currentPlayer.pushTrial(trialN);

        this.oldPlayer = this.currentPlayer;

        this.switchUser();

        if(this.handleTurnEvent != null) {
            this.handleTurnEvent.on( this.oldPlayer,this.currentPlayer);
        }
    }

    public void Restart() {
        this.start = false;

        this.Start();

        this.start = true;

    }

    public void Stop() {

        this.start = false;
    }

    public void Start() {

        Integer n = RandomHelper.randomNumber(0,100);
        if(n > 50) {
            this.currentPlayer = this.player1;
            this.oldPlayer = this.player2;
        }else if(n <= 50) {
            this.currentPlayer = this.player2;
            this.oldPlayer = this.player1;

        }

        this.MagicNumber =  RandomHelper.randomNumber(0,100);

        this.start = true;

        this.handleStartedEvent.on(this.currentPlayer);


    }

    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

    public Player getOldPlayer() {
        return this.oldPlayer;
    }

    public Integer getMagicNumber() {
        return this.MagicNumber;
    }
}
