package com.example.school_project;

import com.example.school_project.helpers.Enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Player {

    protected String Name;

    protected Integer NumberTrials;

    protected List<Integer> NumberTrialsSlected = new ArrayList<Integer>();

    protected Enums.PlayerLevel Level;

    public Player(String playerName, Enums.PlayerLevel level) {

        this.Level = level;
        this.Name = playerName;

    }

    public String getName() {
        return this.Name;
    }
    public Enums.PlayerLevel getLevel() {
        return this.Level;
    }

    public String getLevelToString() {
        return this.Level.toString();
    }

    public void pushTrial(Integer trial) {
        this.NumberTrialsSlected.add(trial);
    }

    public  Integer[] getNumberTrials() {
        return this.NumberTrialsSlected.stream().toArray(Integer[]::new);
    }
}
