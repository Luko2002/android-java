package com.example.school_project;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;


import androidx.activity.EdgeToEdge;

import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.school_project.Activitys.GameActivity;
import com.example.school_project.helpers.Enums;
import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {


    private Button letsGoButton;
    private TextInputEditText inputPlayerName1;
    private TextInputEditText inputPlayerName2;
    private MainActivity This;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);

        setContentView(R.layout.home);

        This = this;

        Spinner spinnerPlayer1 =  findViewById(R.id.spinnerPlayer1Level);
        Spinner spinnerPlayer2 =  findViewById(R.id.spinnerPlayer2Level);

        String[] availablePlayerLevel = Enums.ToArrayString(Enums.PlayerLevel.class);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,availablePlayerLevel);

        spinnerPlayer1.setAdapter(adapter);
        spinnerPlayer2.setAdapter(adapter);



        this.letsGoButton  = findViewById(R.id.letsGo);
        this.inputPlayerName1 =  findViewById(R.id.inputPlayerName1);
        this.inputPlayerName2 =  findViewById(R.id.inputPlayerName2);


        Spinner spinenrPlayer1 =  findViewById(R.id.spinnerPlayer1Level);

        this.letsGoButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String futurPlayerName1 = inputPlayerName1.getText().toString().trim();
               String futurPlayerName2 = inputPlayerName2.getText().toString().trim();

               if(futurPlayerName1.isEmpty() || futurPlayerName2.isEmpty()) {
                   Dialogs CDialog = new Dialogs();
                   CDialog.setTitle("Error").setMessage("Veuillez entrer des noms !.").setPositiveButtonTitle("OK").ShowDialog(getSupportFragmentManager());
                   return;
               }

               if(futurPlayerName1.equals(futurPlayerName2)) {
                   Dialogs CDialog = new Dialogs();
                   CDialog.setTitle("Error").setMessage("Les joueurs ne peuvent pas avoir le même nom !.").setPositiveButtonTitle("OK").ShowDialog(getSupportFragmentManager());
                   return;
               }

               Intent it = new Intent(getApplicationContext(), GameActivity.class);

               String player1Level = spinnerPlayer1.getSelectedItem().toString();
               String player2Level = spinnerPlayer2.getSelectedItem().toString();

               it.putExtra("playerName1", futurPlayerName1);
               it.putExtra("player1Level", player1Level);
               it.putExtra("playerName2", futurPlayerName2);
               it.putExtra("player2Level", player2Level);

               Log.i("start-game", "new game started with "+futurPlayerName1+" vs "+futurPlayerName2);

               startActivity(it);

               finish();
           }
       });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}