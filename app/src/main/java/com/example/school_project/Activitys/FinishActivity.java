package com.example.school_project.Activitys;


import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.school_project.Dialogs;
import com.example.school_project.Player;
import  com.example.school_project.R;
import com.example.school_project.helpers.Enums;
import com.example.school_project.helpers.Game;
import com.example.school_project.interfaces.events;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;


import androidx.activity.EdgeToEdge;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class FinishActivity extends AppCompatActivity {

    private FinishActivity This;
    private TextView thewon;

    private TextView thedefeat;

    private TextView wonTrials;

    private TextView defeatTrials;

    private TextView defeatPlayer;

    private String playerWonName;
    private Integer playerWonTrials;
    private Enums.PlayerLevel playerWonLevel;

    private String playerDefeatName;
    private Integer playerDefeatTrials;
    private Enums.PlayerLevel playerDefeatLevel;

    private void trialWonInternalManage() {

         String message = "";
         if(this.playerWonLevel == Enums.PlayerLevel.FAIBLE) {
             message = "Pas mal pour un débutant !.";
        } else if(this.playerWonLevel == Enums.PlayerLevel.MOYEN) {
             message = "Félicitation !.";
         } if(this.playerWonLevel == Enums.PlayerLevel.HAUT) {
            message = "Une habitude ce jeux !.";
        }

         String trialPls = this.playerWonTrials > 1 ? " coups, " : " coup, ";

         String formattedMessage = "En "+Integer.toString(this.playerWonTrials)+trialPls+message;

         this.wonTrials.setTextColor(Color.parseColor("#75FC3B"));
        this.wonTrials.setText(formattedMessage);
    }

    private void trialDefeatInternalManage() {

        String message = "";
        if(this.playerDefeatLevel == Enums.PlayerLevel.FAIBLE) {
            message = "Pour une prochaine fois...!.";
        } else if(this.playerDefeatLevel == Enums.PlayerLevel.MOYEN) {
            message = "Tu n'as pas le niveau que tu penses... !.";
        } if(this.playerDefeatLevel == Enums.PlayerLevel.HAUT) {
            message = "tu t'es fait écrasé";
        }

        String trialPls = this.playerDefeatTrials > 1 ? " coups, " : " coup, ";

        String formattedMessage = "En "+Integer.toString(this.playerDefeatTrials)+trialPls+message;

        this.defeatTrials.setTextColor(Color.parseColor("#FC3B3B"));
        this.defeatTrials.setText(formattedMessage);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);

        setContentView(R.layout.finish);

        This = this;

        this.thewon = findViewById(R.id.thewon);
        this.wonTrials = findViewById(R.id.trialWonNumber);
        this.defeatPlayer = findViewById(R.id.defeatPlayer);
        this.thedefeat = findViewById(R.id.defeatPlayer);
        this.defeatTrials = findViewById(R.id.trialDefeatNumber);

        Intent it = getIntent();

        this.playerWonName = it.getStringExtra("playerWonName");
        this.playerWonTrials =  it.getIntExtra("playerWonTrials",0);
        this.playerWonLevel =   Enums.PlayerLevel.valueOf(it.getStringExtra("playerWonLevel"));

        this.playerDefeatName = it.getStringExtra("playerDefeatName");
        this.playerDefeatTrials  = (it.getIntExtra("playerDefeatTrials", 1));
        this.playerDefeatLevel =   Enums.PlayerLevel.valueOf(it.getStringExtra("playerDefeatLevel"));

        this.thewon.setText(this.playerWonName);
        this.thedefeat.setText(this.playerDefeatName);

        this.trialWonInternalManage();
        this.trialDefeatInternalManage();

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}