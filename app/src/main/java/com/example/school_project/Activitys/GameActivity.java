package com.example.school_project.Activitys;


import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.school_project.Dialogs;
import com.example.school_project.MainActivity;
import com.example.school_project.Player;
import  com.example.school_project.R;
import com.example.school_project.helpers.Game;
import com.example.school_project.interfaces.events;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;


import androidx.activity.EdgeToEdge;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class GameActivity extends AppCompatActivity {

    private GameActivity This;

    private TextView turnText;

    private TextView textIndication;

    private TextView playerNameTurn;

    private FloatingActionButton buttonOk;

    private TextInputEditText inputNumber;


    private  Thread _threadIndicator;


    private Game game;

   private void indicationInternalManage(Integer n)  {

       if (this.game == null) return;
       this._threadIndicator = new Thread(() -> {


             Integer magicNumber = game.getMagicNumber();
           //  buttonOk.setEnabled(false);
             if (n > magicNumber) {
                 Integer offset = n - magicNumber;
                 Log.i("offset > ", Integer.toString(offset));
                 if (offset > magicNumber) {
                     textIndication.setTextColor(Color.parseColor("#FC3B3B"));
                     Log.i("offset > ", "offset 1 ");
                 }
                 if (offset < 40) {
                     textIndication.setTextColor(Color.parseColor("#FC903B"));
                     Log.i("offset > ", "offset 2 ");
                 }
                 if (offset < 20) {
                     textIndication.setTextColor(Color.parseColor("#FCD93B"));
                     Log.i("offset > ", "offset 3 ");
                 }
                 if (offset < 10) {
                     textIndication.setTextColor(Color.parseColor("#E5FC3B"));
                     Log.i("offset > ", "offset 4 ");
                 }
                 textIndication.setText("C'est plus petit");
             } else if (n < magicNumber) {
                 Integer offset = magicNumber - n;
                 Log.i("offset > ", Integer.toString(offset));
                 if (offset < magicNumber) {
                     textIndication.setTextColor(Color.parseColor("#FC3B3B"));
                     Log.i("offset < ", "offset 1 ");
                 }
                 if (offset < 30) {
                     textIndication.setTextColor(Color.parseColor("#FC903B"));
                     Log.i("offset < ", "offset 2 ");
                 }
                 if (offset < 20) {
                     textIndication.setTextColor(Color.parseColor("#FCD93B"));
                     Log.i("offset < ", "offset 3 ");
                 }
                 if (offset < 10) {
                     textIndication.setTextColor(Color.parseColor("#E5FC3B"));
                     Log.i("offset < ", "offset 4 ");
                 }
                 textIndication.setText("C'est plus grand");
             } else if (n == magicNumber) {
                 textIndication.setTextColor(Color.parseColor("#75FC3B"));
                 textIndication.setText("C'est ça");

                 try {
                     Thread.sleep(2500);
                 } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                 }

                 Intent it = new Intent(getApplicationContext(), FinishActivity.class);

                 Player currentPlayer = game.getCurrentPlayer();
                 Player oldPlayer = game.getOldPlayer();

                 it.putExtra("playerWonName",oldPlayer.getName());
                 it.putExtra("playerWonTrials",oldPlayer.getNumberTrials().length);
                 it.putExtra("playerWonLevel",oldPlayer.getLevel().toString());

                 it.putExtra("playerDefeatName",currentPlayer.getName());
                 it.putExtra("playerDefeatTrials",currentPlayer.getNumberTrials().length);
                 it.putExtra("playerDefeatLevel",currentPlayer.getLevel().toString());

                 startActivity(it);

                 finish();

                 return;
             }
             try {
                 Thread.sleep(2500);
             } catch (InterruptedException e) {
                 Thread.currentThread().interrupt();
             }

 //            buttonOk.setEnabled(true);

            textIndication.setText("");

     });

       this._threadIndicator.start();
   }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);

        setContentView(R.layout.game);

        This = this;

        this.turnText = findViewById(R.id.turnText);
        this.playerNameTurn = findViewById(R.id.playerTurnName);
        this.buttonOk = findViewById(R.id.buttonOk);
        this.textIndication = findViewById(R.id.textIndication);
        this.inputNumber = findViewById(R.id.inputNumber);

        Intent it = getIntent();

        String playerName1 = it.getStringExtra("playerName1");
        String playerName2 = it.getStringExtra("playerName2");

        String player1Level = it.getStringExtra("player1Level");
        String player2Level = it.getStringExtra("player2Level");

        this.game = new Game(playerName1, player1Level, playerName2, player2Level);

        this.buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(_threadIndicator != null) {
                    if(_threadIndicator.isAlive()) {
                        return;
                    }
                }

                String inputNumberStringe = inputNumber.getText().toString();

                if(inputNumberStringe.isEmpty()) {
                    Dialogs CDialog = new Dialogs();
                    CDialog.setTitle("Error").setMessage("Veuillez entrer un nombre !.").setPositiveButtonTitle("OK").ShowDialog(getSupportFragmentManager());
                    return;
                }

                Integer inputPlyaerNumber =  Integer.parseInt(inputNumberStringe);

                game.Turn(inputPlyaerNumber);

                indicationInternalManage(inputPlyaerNumber);


            }
        });


        this.game.setOnGameTurnEvent(new events.GameTurnEvent() {
            @Override
            public void on(Player oldPlayerTurn, Player turnedPlayer) {
                turnText.setText("C'est maintenant le tour de...");
                playerNameTurn.setText(turnedPlayer.getName());
            }
        });
         this.game.setOnGameStarted(new events.GameStartedEvent() {
             @Override
             public void on(Player currentPlayer) {
                 turnText.setText("On commence avec "+currentPlayer.getName()+"...");
             }
         });

        this.game.Start();

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}