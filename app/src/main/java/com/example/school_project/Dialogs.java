package com.example.school_project;

import android.app.AlertDialog;
import android.app.Dialog;
import  androidx.fragment.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class Dialogs extends DialogFragment {

    private String dialogTitle;
    private String titlePositiveButton;

    private String dialogMessage;
    private String titleNegativeButton;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction.
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(this.dialogTitle)
         .setMessage(this.dialogMessage);

        if(this.titlePositiveButton != null) {
            builder.setPositiveButton(this.titlePositiveButton, null);
        }

        if(this.titleNegativeButton != null) {
            builder.setNegativeButton(this.titleNegativeButton, null);
        }

        // Create the AlertDialog object and return it.
        return builder.create();
    }

    public Dialogs setTitle(String title) {
        this.dialogTitle = title;

        return this;
    }

    public Dialogs setMessage(String message) {
        this.dialogMessage = message;

        return this;
    }

    public Dialogs setPositiveButtonTitle(String positiveTitle) {
        this.titlePositiveButton = positiveTitle;

        return this;
    }

    public Dialogs setNegativeButtonTitle(String negativeTitle) {
        this.titleNegativeButton = negativeTitle;

        return this;
    }

    public void ShowDialog(FragmentManager fragment) {
        super.show(fragment,null);
    }
}
